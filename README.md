# Machine Learning Engineer Nanodegree
## Specializations
## Project: Capstone Proposal and Capstone Project

**Requirements**
Python 2.5+
sklearn
matplotlib
scipy
numpy
pandas
Jupyter Notebook

**Files**

Report - include information about the project. Analysis and results can be found there

Proposal - original proposal for the project

tables - include some data and visualisations collected throughout the project. Data is not intender for a review and exist as the way to keep notes

capstone_exploration.ipynb - file with data analysis code

capstone_model.ipynb - file with code for final model and results



datasets - included required datasets. Archivve needs to be exctacted if you want to use on your machine. Souces are included in proposal and report.

logs - folder with text files, that include testing information that was developed during testing period.